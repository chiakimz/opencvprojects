#pragma once
#include <iostream>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

cv::Mat gauss(cv::Mat image) {
	const double PI = 3.14;
	double sigma = 1.4;
	double r, s = 2.0 * sigma * sigma;
	const int kernelWidth = 5;
	const int kernelHeight = 5;

	float kernelArray[kernelWidth][kernelHeight];

	float total = 0.0;


	for (int row = 0; row < kernelWidth; row++) {
		for (int col = 0; col < kernelHeight; col++) {
			r = sqrt(row * row + col * col);
			kernelArray[row][col] = (exp(-(r * r) / s)) / (PI * s);
			total += kernelArray[row][col];
		}
	}

	for (int row = 0; row < kernelWidth; ++row) {
		for (int col = 0; col < kernelHeight; ++col) {
			kernelArray[row][col] /= total;
		}
	}

	cv::Mat grayScaleImage(image.size(), CV_8UC1);

	cv::Mat finalImage(image.size(), CV_8UC1);
	cvtColor(image, grayScaleImage, CV_RGB2GRAY);

	int rows = grayScaleImage.rows;
	int cols = grayScaleImage.cols;

	int verticalImageBound = (kernelHeight - 1) / 2;
	int horizontalImageBound = (kernelWidth - 1) / 2;

	for (int row = 0 + verticalImageBound; row < rows - verticalImageBound; ++row) {
		for (int col = 0 + horizontalImageBound; col < cols - verticalImageBound; ++col) {
			float total = 0.0;

			for (int kRow = 0; kRow < kernelHeight; ++kRow) {
				for (int kCol = 0; kCol < kernelWidth; ++kCol) {
					float pixel = grayScaleImage.at<uchar>(kRow + row - verticalImageBound, kCol + col - horizontalImageBound) * kernelArray[kRow][kCol];
					total += pixel;
				}
			}
			finalImage.at<uchar>(row, col) = cvRound(total);
		}
	}

	return finalImage;
}

int gradientX(cv::Mat image, int x, int y) {
	return image.at <uchar>(y - 1, x - 1) +
		2 * image.at<uchar>(y, x - 1) +
		image.at<uchar>(y + 1, x - 1) -
		image.at<uchar>(y - 1, x + 1) -
		2 * image.at<uchar>(y, x + 1) -
		image.at<uchar>(y + 1, x + 1);
}

int gradientY(cv::Mat image, int x, int y) {
	return image.at<uchar>(y - 1, x - 1) +
		2 * image.at<uchar>(y - 1, x) +
		image.at<uchar>(y - 1, x + 1) -
		image.at<uchar>(y + 1, x - 1) -
		2 * image.at<uchar>(y + 1, x) -
		image.at<uchar>(y + 1, x + 1);
}
cv::Mat gradientXsobel(cv::Mat image) {
	int Gx, Gy, sum;
	cv::Mat imCloneX = cv::Mat(image.rows, image.cols, CV_8U, float(0.0));

	for (int y = 1; y < image.rows - 1; y++) {
		for (int x = 1; x < image.cols - 1; x++) {
			Gx = gradientX(image, x, y);
			sum = abs(Gx);
			sum = sum > 255 ? 255 : sum;
			sum = sum < 0 ? 0 : sum;
			imCloneX.at<uchar>(y, x) = sum;
		}
	}
	return imCloneX;
}

cv::Mat gradientYsobel(cv::Mat image) {
	cv::Mat imCloneY = cv::Mat(image.rows, image.cols, CV_8U, float(0.0));
	int Gx, Gy, sum;

	for (int y = 1; y < image.rows - 1; y++) {
		for (int x = 1; x < image.cols - 1; x++) {
			Gy = gradientY(image, x, y);
			sum = abs(Gy);
			sum = sum > 255 ? 255 : sum;
			sum = sum < 0 ? 0 : sum;
			imCloneY.at<uchar>(y, x) = sum;
		}
	}
	return imCloneY;
}

cv::Mat sobel(cv::Mat image) {
	cv::Mat imClone = cv::Mat(image.rows, image.cols, CV_8U, float(0.0));
	int Gx, Gy, sum;

	for (int y = 1; y < image.rows - 1; y++) {
		for (int x = 1; x < image.cols - 1; x++) {
			Gx = gradientX(image, x, y);
			Gy = gradientY(image, x, y);
			sum = abs(Gx) + abs(Gy);
			//prevents integer to go over 255 or below 0
			sum = sum > 255 ? 255 : sum;
			sum = sum < 0 ? 0 : sum;
			imClone.at<uchar>(y, x) = sum;
		}
	}
	return imClone;
}

cv::Mat cannyDetect(cv::Mat image, int upperThreshold, int lowerThreshold, double size = 3) {
	cv::Mat thisImage = gauss(image);
	cv::Mat thisImageX = gauss(image);
	cv::Mat thisImageY = gauss(image);
	//calculate gradient magnitudes and directions
	cv::Mat gradientMagnitudeX = gradientXsobel(thisImageX);
	gradientMagnitudeX.convertTo(gradientMagnitudeX, CV_32F);
	cv::Mat gradientMagnitudeY = gradientYsobel(thisImageY);
	gradientMagnitudeY.convertTo(gradientMagnitudeY, CV_32F);
	thisImage = sobel(thisImage);
	//calculate the slope at each point by dividing the Y derivative by X
	cv::Mat direction = cv::Mat(thisImage.rows, thisImage.cols, CV_32F);
	cv::divide(gradientMagnitudeY, gradientMagnitudeX, direction);
	//calculate the magnitude of the gradient at each pixel
	cv::Mat sum = cv::Mat(thisImage.rows, thisImage.cols, CV_64F);
	cv::Mat prodX = cv::Mat(thisImage.rows, thisImage.cols, CV_64F);
	cv::Mat prodY = cv::Mat(thisImage.rows, thisImage.cols, CV_64F);
	cv::multiply(gradientMagnitudeX, gradientMagnitudeX, prodX);
	cv::multiply(gradientMagnitudeY, gradientMagnitudeY, prodY);
	sum = prodX + prodY;
	cv::sqrt(sum, sum);

	//CALCULATE LOCAL MAXIMA AND APPLY NON-MAXIMUM SUPRESSION
	//figure out points that definitely line on edges = points whose gradient magnitude is greater than upper threshold and are maxima
	cv::Mat returnImage = cv::Mat(image.rows, image.cols, CV_8U);
	//returnImage is the image that wil have all canny edges. we've initialised all iterators for the magnitude, direction and return images
	returnImage.setTo(cv::Scalar(0)); //initialise image to zero
	//initialise iterators
	cv::MatIterator_<float>magnitudeIterator = sum.begin<float>();
	cv::MatIterator_<float>directionIterator = direction.begin<float>();
	cv::MatIterator_<uchar>returnImageIterator = returnImage.begin<uchar>();
	cv::MatIterator_<float>magnitudeIteratorLast = sum.end<float>();
	//start going through each pixel. we store the current pixel, calculate the gradient direction in terms
	//~of degrees and store it back, then if the pixel's gradient magnitude is not high enough we skip it.
	for (; magnitudeIterator != magnitudeIteratorLast; ++directionIterator, ++returnImageIterator, ++magnitudeIterator) {
		//Thomas says: inner_loop(*itDirection, *itRet, *itMag) make this a function to make it obvious what its doing
		const cv::Point currentPixel = returnImageIterator.pos();
		float currentDirection = atan(*directionIterator) * 180 / 3.142;
		while (currentDirection < 0) {
			currentDirection += 180;
		}
		*directionIterator = currentDirection;
		if (*magnitudeIterator < upperThreshold) {
			continue;
		}

		bool isThisPixelEdge = true; //indicates if the current pixel is an edge or not

		//If the gradient direction is between 112.5 and 157.5 degrees, then it is rounded off to 135 degrees
		if (currentDirection > 112.5 && currentDirection <= 157.5) {
			if (currentPixel.y > 0 && currentPixel.x < thisImage.cols - 1 && *magnitudeIterator <= sum.at<float>(currentPixel.y - 1, currentPixel.x + 1)) isThisPixelEdge = false;
			if (currentPixel.y < thisImage.rows - 1 && currentPixel.x > 0 && *magnitudeIterator <= sum.at<float>(currentPixel.y + 1, currentPixel.x - 1)) isThisPixelEdge = false;
		}
		//If the gradient direction is between 67.5 and 112.5 degrees, then it is rounded off to 90 degrees
		else if (currentDirection > 67.5 && currentDirection <= 112.5) {
			if (currentPixel.y > 0 && *magnitudeIterator <= sum.at<float>(currentPixel.y - 1, currentPixel.x))
				isThisPixelEdge = false;
			if (currentPixel.y < thisImage.rows - 1 && *magnitudeIterator <= sum.at<float>(currentPixel.y + 1, currentPixel.x))
				isThisPixelEdge = false;
		}
		//If the gradient direction is between 22.5 and 67.5 degrees, then it is rounded off to 45 degrees
		else if (currentDirection > 22.5 && currentDirection <= 67.5) {
			if (currentPixel.y > 0 && currentPixel.x > 0 && *magnitudeIterator <= sum.at<float>(currentPixel.y - 1, currentPixel.x - 1))
				isThisPixelEdge = false;
			if (currentPixel.y < thisImage.rows - 1 && currentPixel.x < thisImage.cols - 1 && *magnitudeIterator <= sum.at<float>(currentPixel.y + 1, currentPixel.x + 1))
				isThisPixelEdge = false;
		}
		else {
			if (currentPixel.x > 0 && *magnitudeIterator <= sum.at<float>(currentPixel.y, currentPixel.x - 1))
				isThisPixelEdge = false;
			if (currentPixel.x < thisImage.cols - 1 && *magnitudeIterator <= sum.at<float>(currentPixel.y, currentPixel.x + 1))
				isThisPixelEdge = false;
		}
		if (isThisPixelEdge) {
			*returnImageIterator = 255; //mark the pixel white if the current pixel is a define edge
		}
	}
	//Hysteresis threshold

	//if new edge pixels are found, imageChanged will be changed to true
	bool imageChanged = true;
	int i = 0;
	while (imageChanged) {
		imageChanged = false;
		printf("Iteration %d\ ", i);
		i++;
		//initialise iterators again
		magnitudeIterator = sum.begin<float>();
		directionIterator = direction.begin<float>();
		returnImageIterator = returnImage.begin<uchar>();
		magnitudeIteratorLast = sum.end<float>();

		for (; magnitudeIterator != magnitudeIteratorLast; ++magnitudeIterator, ++directionIterator, returnImageIterator) {
			//extract the current pixels gradient direction and position
			cv::Point currentPixel = returnImageIterator.pos();
			if (currentPixel.x < 2 || currentPixel.x >= image.cols - 2 || currentPixel.y < 2 || currentPixel.y >= image.rows - 2)
				continue;
			float currentDirection = *directionIterator;
			//if we're near the edge, skip those pixels, then the intencity is changed to arbitrary int 64
			if (*returnImageIterator == 255) { //it's a fresh edge pixel
				*returnImageIterator = (uchar)64;
				if (currentDirection > 112.5 && currentDirection <= 157.5) {
					if (currentPixel.y > 0 && currentPixel.x > 0) {
						//apply double threshold and hysteresis
						if (lowerThreshold <= sum.at<float>(currentPixel.y - 1, currentPixel.x - 1) &&
							returnImage.at<uchar>(currentPixel.y - 1, currentPixel.x - 1) != 64 &&
							direction.at<float>(currentPixel.y - 1, currentPixel.x - 1) > 112.5 &&
							direction.at<float>(currentPixel.y - 1, currentPixel.x - 1) <= 157.5 &&
							sum.at<float>(currentPixel.y - 1, currentPixel.x - 1) > sum.at<float>(currentPixel.y - 2, currentPixel.x) &&
							sum.at<float>(currentPixel.y - 1, currentPixel.x - 1) > sum.at<float>(currentPixel.y, currentPixel.x - 2)) {
							returnImage.ptr<uchar>(currentPixel.y - 1, currentPixel.x - 1)[0] = 255;
							imageChanged = true;
						}

					}
					if (currentPixel.y < thisImage.rows - 1 && currentPixel.x < thisImage.cols - 1) {
						if (lowerThreshold <= sum.at<float>(cv::Point(currentPixel.x + 1, currentPixel.y + 1)) &&
							returnImage.at<uchar>(currentPixel.y + 1, currentPixel.x + 1) != 64 &&
							direction.at<float>(currentPixel.y + 1, currentPixel.x + 1) > 112.5 &&
							direction.at<float>(currentPixel.y + 1, currentPixel.x + 1) <= 157.5 &&
							sum.at<float>(currentPixel.y - 1, currentPixel.x - 1) > sum.at<float>(currentPixel.y + 2, currentPixel.x) &&
							sum.at<float>(currentPixel.y - 1, currentPixel.x - 1) > sum.at<float>(currentPixel.y, currentPixel.x + 2)) {
							returnImage.ptr<uchar>(currentPixel.y + 1, currentPixel.x + 1)[0] = 255;
							imageChanged = true;
						}
					}
				}
				else if (currentDirection > 67.5 && currentDirection <= 112.5) {
					if (currentPixel.x > 0) {
						if (lowerThreshold <= sum.at<float>(cv::Point(currentPixel.x - 1, currentPixel.y)) &&
							returnImage.at<uchar>(currentPixel.y, currentPixel.x - 1) != 64 &&
							direction.at<float>(currentPixel.y, currentPixel.x - 1) > 67.5 &&
							direction.at<float>(currentPixel.y, currentPixel.x - 1) <= 112.5 &&
							sum.at<float>(currentPixel.y, currentPixel.x - 1) > sum.at<float>(currentPixel.y - 1, currentPixel.x - 1) &&
							sum.at<float>(currentPixel.y, currentPixel.x - 1) > sum.at<float>(currentPixel.y + 1, currentPixel.x - 1)) {
							returnImage.ptr<uchar>(currentPixel.y, currentPixel.x - 1)[0] = 255;
							imageChanged = true;
						}
					}
					if (currentPixel.x < thisImage.cols - 1) {
						if (lowerThreshold <= sum.at<float>(cv::Point(currentPixel.x + 1, currentPixel.y)) &&
							returnImage.at<uchar>(currentPixel.y, currentPixel.x + 1) != 64 &&
							direction.at<float>(currentPixel.y, currentPixel.x + 1) > 67.5 &&
							direction.at<float>(currentPixel.y, currentPixel.x + 1) <= 112.5 &&
							sum.at<float>(currentPixel.y, currentPixel.x + 1) > sum.at<float>(currentPixel.y - 1, currentPixel.x + 1) &&
							sum.at<float>(currentPixel.y, currentPixel.x + 1) > sum.at<float>(currentPixel.y + 1, currentPixel.x + 1)) {
							returnImage.ptr<uchar>(currentPixel.y, currentPixel.x + 1)[0] = 255;
							imageChanged = true;
						}
					}
				}
				else if (currentDirection > 22.5 && currentDirection <= 67.5) {
					if (currentPixel.y > 0 && currentPixel.x < thisImage.cols - 1) {
						if (lowerThreshold <= sum.at<float>(cv::Point(currentPixel.x + 1, currentPixel.y - 1)) &&
							returnImage.at<uchar>(currentPixel.y - 1, currentPixel.x + 1) != 64 &&
							direction.at<float>(currentPixel.y - 1, currentPixel.x + 1) > 22.5 &&
							direction.at<float>(currentPixel.y - 1, currentPixel.x + 1) <= 67.5 &&
							sum.at<float>(currentPixel.y - 1, currentPixel.x + 1) > sum.at<float>(currentPixel.y - 2, currentPixel.x) &&
							sum.at<float>(currentPixel.y - 1, currentPixel.x + 1) > sum.at<float>(currentPixel.y, currentPixel.x + 2)) {
							returnImage.ptr<uchar>(currentPixel.y - 1, currentPixel.x + 1)[0] = 255;
							imageChanged = true;
						}
					}
					if (currentPixel.y < thisImage.rows - 1 && currentPixel.x > 0) {
						if (lowerThreshold <= sum.at<float>(cv::Point(currentPixel.x - 1, currentPixel.y + 1)) &&
							returnImage.at<uchar>(currentPixel.y + 1, currentPixel.x - 1) != 64 &&
							direction.at<float>(currentPixel.y + 1, currentPixel.x - 1) > 22.5 &&
							direction.at<float>(currentPixel.y + 1, currentPixel.x - 1) <= 67.5 &&
							sum.at<float>(currentPixel.y + 1, currentPixel.x - 1) > sum.at<float>(currentPixel.y, currentPixel.x - 2) &&
							sum.at<float>(currentPixel.y + 1, currentPixel.x - 1) > sum.at<float>(currentPixel.y + 2, currentPixel.x)) {
							returnImage.ptr<uchar>(currentPixel.y + 1, currentPixel.x - 1)[0] = 255;
							imageChanged = true;
						}
					}
				}
				else {
					if (currentPixel.y > 0) {
						if (lowerThreshold <= sum.at<float>(cv::Point(currentPixel.x, currentPixel.y - 1)) &&
							returnImage.at<uchar>(currentPixel.y - 1, currentPixel.x) != 64 &&
							(direction.at<float>(currentPixel.y - 1, currentPixel.x) < 22.5 ||
								direction.at<float>(currentPixel.y - 1, currentPixel.x) >= 157.5) &&
							sum.at<float>(currentPixel.y - 1, currentPixel.x) > sum.at<float>(currentPixel.y - 1, currentPixel.x - 1) &&
							sum.at<float>(currentPixel.y - 1, currentPixel.x) > sum.at<float>(currentPixel.y - 1, currentPixel.x + 2)) {
							returnImage.ptr<uchar>(currentPixel.y - 1, currentPixel.x)[0] = 255;
							imageChanged = true;
						}
					}
					if (currentPixel.y < thisImage.rows - 1) {
						if (lowerThreshold <= sum.at<float>(cv::Point(currentPixel.x, currentPixel.y + 1)) &&
							returnImage.at<uchar>(currentPixel.y + 1, currentPixel.x) != 64 &&
							(direction.at<float>(currentPixel.y + 1, currentPixel.x) < 22.5 ||
								direction.at<float>(currentPixel.y + 1, currentPixel.x) >= 157.5) &&
							sum.at<float>(currentPixel.y + 1, currentPixel.x) > sum.at<float>(currentPixel.y + 1, currentPixel.x - 1) &&
							sum.at<float>(currentPixel.y + 1, currentPixel.x) > sum.at<float>(currentPixel.y + 1, currentPixel.x + 1)) {
							returnImage.ptr<uchar>(currentPixel.y + 1, currentPixel.x)[0] = 255;
							imageChanged = true;
						}
					}
				}
			}
		}
	}
	//at this point all edge pixels are marked as 64, below is to change that into 255
	cv::MatIterator_<uchar>current = returnImage.begin<uchar>();
	cv::MatIterator_<uchar>final = returnImage.end<uchar>();
	for (; current != final; ++current) {
		if (*current == 64)
			*current = 255;
	}

	return returnImage;
}

