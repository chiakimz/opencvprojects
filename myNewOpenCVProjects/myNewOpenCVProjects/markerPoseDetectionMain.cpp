//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc/imgproc.hpp"
#pragma once
#include "cannyEdgeDetection.cpp"
//#include <iostream>

int main() {
	cv::Mat image = cv::imread("toolkit.jpg");
	cv::Mat canniedImage = cannyDetect(image, 180, 80, 3);
	cv::namedWindow("Canny Edge Detected Image");
	cv::imshow("Canny Edge Detected Image", canniedImage);
	cv::waitKey();
	return 0;
}