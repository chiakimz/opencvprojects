#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
//function declarations
void drawAxis(cv::Mat&, cv::Point, cv::Point, cv::Scalar, const float);
double getOrientation(const std::vector<cv::Point> &, cv::Mat&);

void drawAxis(cv::Mat& image, cv::Point point1, cv::Point point2, cv::Scalar colour, const float scale = 0.2) {
	double angle = atan2((double)point1.y - point2.y, (double)point1.x - point2.x); //atan2: computes arc tangent with two parameters
	double hypotenuse = sqrt((double)(point1.y - point2.y) * (point1.y - point2.y) + (point1.x - point2.x) * (point1.x - point2.x));  //keisen
	//lengthen the arrow by a factor of scale
	point2.x = (int)(point1.x - scale * hypotenuse * cos(angle));
	point2.y = (int)(point1.y - scale * hypotenuse * sin(angle));
	line(image, point1, point2, colour, 1, cv::LINE_AA);

	//create the arrow hooks
	point1.x = (int)(point2.x + 9 * cos(angle + CV_PI / 4));
	point1.y = (int)(point2.y + 9 * sin(angle + CV_PI / 4));
	line(image, point1, point2, colour, 1, cv::LINE_AA);

	point1.x = (int)(point2.x + 9 * cos(angle - CV_PI / 4));
	point1.y = (int)(point2.y + 9 * sin(angle - CV_PI / 4));
	line(image, point1, point2, colour, 1, cv::LINE_AA);
}

double getOrientation(const std::vector<cv::Point> &points, cv::Mat &image) {
	//construct a buffer used by the pca analysis
	int size = static_cast<int>(points.size());
	cv::Mat data_points = cv::Mat(size, 2, CV_64F);
	for (int i = 0; i < data_points.rows; i++) {
		data_points.at<double>(i, 0) = points[i].x;
		data_points.at<double>(i, 1) = points[i].y;
	}
	//perform pca analysis
	cv::PCA pca_analysis(data_points, cv::Mat(), cv::PCA::DATA_AS_ROW);
	//store the centre of the object
	cv::Point centre = cv::Point(static_cast<int>(pca_analysis.mean.at<double>(0, 0)),
		static_cast<int>(pca_analysis.mean.at<double>(0, 1)));
	//store the eigenvalues and eigenvectors
	std::vector<cv::Point2d> eigen_vecs(2);
	std::vector<double> eigen_val(2);
	for (int i = 0; i < 2; i++) {
		eigen_vecs[i] = cv::Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
			pca_analysis.eigenvectors.at<double>(i, 1));
		eigen_val[i] = pca_analysis.eigenvalues.at<double>(i);
	}

	//draw the principal componets
	cv::circle(image, centre, 3, cv::Scalar(255, 0, 255), 2);
	cv::Point pointA = centre + 0.02 * cv::Point(static_cast<int>(eigen_vecs[0].x * eigen_val[0]), static_cast<int>(eigen_vecs[0].y * eigen_val[0]));
	cv::Point pointB = centre - 0.02 * cv::Point(static_cast<int>(eigen_vecs[1].x * eigen_val[1]), static_cast<int>(eigen_vecs[1].y * eigen_val[1]));
	drawAxis(image, centre, pointA, cv::Scalar(0, 255, 0), 1);
	drawAxis(image, centre, pointB, cv::Scalar(255, 255, 0), 5);

	double angle = atan2(eigen_vecs[0].y, eigen_vecs[0].x); //orientation in radiants

	return angle;
}

int main() {
	cv::Mat image = cv::imread("toolkit.jpg");

	if (image.empty()) {
		std::cout << "Problem loading image!" << std::endl;
		return -1;
	}
	cv::imshow("image", image);
	//convert to grayscale image
	cv::Mat gray;
	cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);
	//convert to binary image
	cv::Mat grayImage;
	cv::threshold(gray, grayImage, 50, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
	//find all the contours in the thresholded image
	std::vector<std::vector<cv::Point>>contours;
	findContours(grayImage, contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);

	for (size_t i = 0; i < contours.size(); i++) {
		//calculate the area of each contour
		double area = cv::contourArea(contours[i]);
		//ignore contours that are too small or too large
		if (area < 1e2 || 1e5 < area) continue;
		//draw each contour only for visualisation purposes
		cv::drawContours(image, contours, static_cast<int>(i), cv::Scalar(0, 0, 255), 2);
		//find the orientation for each shape
		getOrientation(contours[i], image);

	}
	cv::imshow("output", image);
	cv::waitKey();
	return 0;
}
